# Description : This code purpose is to cluster photo from multiple folder.
# Contributor : Thariq Ramadhan
# Source : https://github.com/moondra2017/Computer-Vision, http://dlib.net/face_clustering.py.html

from argparse import ArgumentParser
import os
import itertools
from imageio import imread
import cv2
from scipy.spatial import distance
import numpy as np
import shutil  
import sys
import dlib
import glob
import shutil

# TO USE : 
# python3 face_clustering_recursive.py \
# --input_dir <your-unprocessed-dataset-path> \
# --output_dir <your-processed-dataset-path> \
# --predictor <your-trained-facial-shape-predictor>\
# --recognition <your-trained-recognition-model>

parser = ArgumentParser()
parser.add_argument("--input_dir", type=str, default=None,
                    help="Path of raw dataset")
parser.add_argument("--output_dir", type=str, default=None,
                    help="Path of output directory")
parser.add_argument("--predictor", type=str, default=None,
                    help="The trained facial shape predictor")
parser.add_argument("--recognition", type=str, default=None,
                    help="The trained recognition model")
args = parser.parse_args()


def main():
    # Copy all folder in input to output folder
    shutil.copytree(args.input_dir, args.output_dir + "/face_filter_similar" )

    f_similar_folders = os.listdir(args.output_dir + "/face_filter_similar")
    f_all = args.output_dir + "/all_face"
    try:
        os.mkdir(f_all)
        print("Directory " , f_all ,  " Created ") 
    except FileExistsError:
        print("Directory " , f_all ,  " already exists")
    
    # Filter similar face and copy it to all folder
    i = 0
    for f_similar_folder in f_similar_folders:
        print("Face filter similar folder " + f_similar_folder)
        face_folder = args.output_dir + "/face_filter_similar/" + f_similar_folder
        face_filter_similar(face_folder, 0.3)

        image_files = os.listdir(face_folder)
        for image_file in image_files:
            print("Copy face image in " + f_similar_folder + " to all face folder")
            os.rename(face_folder + "/" + image_file, f_all + "/" + str(i) + "_" + image_file )

        i = i + 1
    
    # Face cluster
    print("Face Clustering with threshold 0.5 with minimal images 10")
    face_clustering(predictor_path = args.predictor, 
                    face_rec_model_path = args.recognition, 
                    input_folder_path = f_all, 
                    output_folder_path = args.output_dir + "/face_cluster",
                    threshold = 0.5,
                    min_image = 10)
    
    # Recursive face cluster
    cluster_folders = os.listdir(args.output_dir + "/face_cluster")
    for label in cluster_folders:
        dir_name = args.output_dir + "/face_cluster/" + label
        count_file = sum([len(files) for r, d, files in os.walk(dir_name)])
        if (count_file > 90):
            print("Face Clustering label " + label + " with threshold 0.4 with minimal images 5 because contains more than 90 images")
            face_clustering(predictor_path = args.predictor, 
                            face_rec_model_path = args.recognition, 
                            input_folder_path = dir_name, 
                            output_folder_path = args.output_dir + "/face_cluster",
                            threshold = 0.4,
                            min_image = 5,
                            inheritance= label)
            try:
                print("Delete label " + label + " because contains more than 90 images")
                shutil.rmtree(dir_name)
            except OSError as e:
                print("Error: %s : %s" % (dir_name, e.strerror))
            
def face_filter_similar(src, threshold):

    def filter_images(images):
        image_list = []
        for image in images:
            try:
                assert imread(image).shape[2] == 3
                image_list.append(image)
            except  AssertionError as e:
                print(e)
        return image_list

    def difference_score_dict(image_list):
        ds_dict = {}
        duplicates = []
        i = 1
        for image in image_list:
            print("Calculate difference score image-" + str(i))
            i = i + 1
            ds = difference_score(image)
            
            if image not in ds_dict:
                ds_dict[image] = ds
            else:
                duplicates.append((image, ds_dict[image]) )
        return  duplicates, ds_dict

    def difference_score(image, height = 30, width = 30):
        gray = img_gray(image)
        row_res, col_res = resize(gray, height, width)
        difference = intensity_diff(row_res, col_res)   
        return difference

    def img_gray(image):
        image = imread(image)
        return np.average(image, weights=[0.299, 0.587, 0.114], axis=2)

    def resize(image, height=30, width=30):
        row_res = cv2.resize(image,(height, width), interpolation = cv2.INTER_AREA).flatten()
        col_res = cv2.resize(image,(height, width), interpolation = cv2.INTER_AREA).flatten('F')
        return row_res, col_res

    def intensity_diff(row_res, col_res):
        difference_row = np.diff(row_res)
        difference_col = np.diff(col_res)
        difference_row = difference_row > 0
        difference_col = difference_col > 0
        return np.vstack((difference_row, difference_col)).flatten()

    def hamming_distance(image, image2):
        score = distance.hamming(image, image2)
        return score

    owd = os.getcwd()
    print("Remove duplicate in directory " + src)
    os.chdir(src)
    image_files = os.listdir()
    image_files = filter_images(image_files)
    duplicates, ds_dict = difference_score_dict(image_files)

    for k1,k2 in itertools.combinations(ds_dict, 2):
        if hamming_distance(ds_dict[k1], ds_dict[k2])< threshold:
            duplicates.append((k1,k2))

    for file_names in duplicates:
        try:
            print("Remove duplicate " + file_names[0])
            os.remove(file_names[0])
        except:
            pass

    os.chdir(owd)

def face_clustering(predictor_path, 
                    face_rec_model_path, 
                    input_folder_path, 
                    output_folder_path, 
                    threshold, 
                    min_image,
                    inheritance = None):

    # Load all the models we need: a detector to find the faces, a shape predictor
    # to find face landmarks so we can precisely localize the face, and finally the
    # face recognition model.
    detector = dlib.get_frontal_face_detector()
    sp = dlib.shape_predictor(predictor_path)
    facerec = dlib.face_recognition_model_v1(face_rec_model_path)

    descriptors = []
    images = []

    # Now find all the faces and compute 128D face descriptors for each face.
    for f in glob.glob(os.path.join(input_folder_path, "*.jpg")):
        print("Processing file: {}".format(f))
        img = dlib.load_rgb_image(f)

        # Ask the detector to find the bounding boxes of each face. The 1 in the
        # second argument indicates that we should upsample the image 1 time. This
        # will make everything bigger and allow us to detect more faces.
        dets = detector(img, 1)
        print("Number of faces detected: {}".format(len(dets)))

        # Now process each face we found.
        for k, d in enumerate(dets):
            # Get the landmarks/parts for the face in box d.
            shape = sp(img, d)

            # Compute the 128D vector that describes the face in img identified by
            # shape.  
            face_descriptor = facerec.compute_face_descriptor(img, shape)
            descriptors.append(face_descriptor)
            images.append((img, shape))

    # Now let's cluster the faces.  
    labels = dlib.chinese_whispers_clustering(descriptors, threshold)
    num_classes = len(set(labels))
    print("Number of clusters: {}".format(num_classes))

    # Make directory for label
    for label in labels:
        if inheritance :
            dir_name = output_folder_path + "/" + str(inheritance) + "_" + str(label)
        else :
            dir_name = output_folder_path + "/" + str(label)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

    # Copy image to label directory
    for (i, image) in enumerate(images):
        img, shape = image
        if inheritance :
            file_path = os.path.join(output_folder_path + "/" + str(inheritance) + "_" + str(labels[i]), "face_" + str(i))
        else :
            file_path = os.path.join(output_folder_path + "/" + str(labels[i]), "face_" + str(i))
        
        # The size and padding arguments are optional with default size=150x150 and padding=0.25
        dlib.save_face_chip(img, shape, file_path, size=150, padding=0.25)

    # Delete file if less than min_image
    output_folders = os.listdir(output_folder_path)
    for output_label in output_folders:
        dir_name = output_folder_path + "/" + output_label
        count_file = sum([len(files) for r, d, files in os.walk(dir_name)])
        if (count_file < min_image):
            try:
                print("Delete label " + output_label + " because contains image below " + str(min_image))
                shutil.rmtree(dir_name)
            except OSError as e:
                print("Error: %s : %s" % (dir_name, e.strerror))
             

if __name__ == "__main__":
    main()

