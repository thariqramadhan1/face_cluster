# DBSCA
This project purpose is to cluster simliar face in a folder full of images with DBSCA method.
To use this code:

-If use GPU python `FaceCluster.py --dataset dataset --encodings encodings.pickle`
-If use CPU python `FaceCluster.py --dataset dataset --encodings encodings.pickle --detection-method hog`

# Chinese Whispers
This project purpose is to cluster similiar face in a folder full of images with chinese whispers method.
To use this code:
`face_clustering.py shape_predictor_5_face_landmarks.dat dlib_face_recognition_resnet_model_v1.dat ../examples/faces output_folder`

# Recursive Face cluster
This project purpose is to cluster similiar face in a folder of folder full of images, filter similar face with hamming distance and then cluster with chinese whispers method.

![diagram](https://gitlab.com/thariqramadhan1/face_cluster/-/raw/master/Recursive%20Face%20Cluster/diagram.png)

To use this code:
```
python face_clustering_recursive.py \
--input_dir <your-unprocessed-dataset-path> \
--output_dir <your-processed-dataset-path> \
--predictor <your-trained-facial-shape-predictor>\
--recognition <your-trained-recognition-model>
```

or 

`python face_clustering_recursive.py --input_dir dataset --output_dir out --predictor shape_predictor_5_face_landmarks.dat --recognition dlib_face_recognition_resnet_model_v1.dat`

# Face Cluster
This is the lastest code for this project. Because change of dataset(fix size and remove small photo) recursive is not needed.
![diagram](https://gitlab.com/thariqramadhan1/face_cluster/-/raw/master/Face%20Cluster/Diagram.png)

Add face blur to filter blur face. Download model in here https://drive.google.com/open?id=1B8d7nfUgfUtSt7fgzuLubukWy_A-6lSp.

To use this code:
```
# TO USE : 
# python face_clustering.py \
# --input_dir <your-unprocessed-dataset-path> \
# --output_dir <your-processed-dataset-path> \
# --predictor <your-trained-facial-shape-predictor>\
# --recognition <your-trained-recognition-model>
# --model <your-model>\
```

Change threshold of face similar threshold and face cluster threshold to optimal output.